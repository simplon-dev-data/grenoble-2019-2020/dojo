# Simplon Dev Data: Dojo

> Getting your head around programming requires only one thing: TRAINING AGAIN AND AGAIN!

You will find several `jupyter` notebooks containing exercises/problems around a single concept.
Each notebook will display only the intent of each problem, their respective solutions being hidden right below
(you can toggle it to check your result).
Each problem can be solved in multiple ways so be ready to experiment and get your hands dirty!

## Dev setup

```
pipenv install
jupyter contrib nbextension install --sys-prefix
jupyter nbextension enable collapsible_headings/main
jupyter nbextension enable hide_input/main
```

## Render HTML

```
inv convert-notebooks
```
