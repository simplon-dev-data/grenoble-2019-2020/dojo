import os

from invoke import task
from markdown import markdown


@task
def convert_notebooks(ctx):
    notebooks_path = "notebooks"
    output_path = "dist"
    output_basename = "index"

    md_index = """
# Simplon Dev Data: Dojo

> Getting your head around programming requires only one thing: TRAINING AGAIN AND AGAIN!

You will find several `jupyter` notebooks containing exercises/problems around a single concept.
Each notebook will display only the intent of each problem, their respective solutions being hidden right below
(you can toggle it to check your result).
Each problem can be solved in multiple ways so be ready to experiment and get your hands dirty!

Original repository: [simplon-dev-data/dojo](https://gitlab.com/simplon-dev-data/dojo)

## Available notebooks
    """

    nb_files = []
    for root, dirs, files in os.walk(notebooks_path):
        if len(files) > 0:
            for f in files:
                fname, fext = os.path.splitext(f)
                if fext == ".ipynb":
                    nb_files.append({"path": root, "filename": fname, "extension": fext})

    for f in nb_files:
        fbase = os.path.join(f["path"], f["filename"])
        fin = fbase + f["extension"]
        fout = os.path.join(output_path, fbase)
        ctx.run(
            f"""
            jupyter nbconvert \
                --template=nbextensions \
                --to=html \
                --output-dir={fout} \
                --output={output_basename} {fin}
            """
        )
        md_index += f"\n- [{fbase}](/dojo/{fbase}/)"

    md_html = markdown(md_index)
    md_path = os.path.join(output_path, "index.html")
    with open(md_path, "w") as fp:
        fp.write(md_html)
